# TestAutomation_Starter

## Objective

테스트 자동화 시작반 - 소스 공유장소.

## Folder schema

|   Folders            |         Comments                        |
|---------------------|-----------------------------------------|
| /src/test/java       | Test scripts                            |
| /src/test/resources | Test configuration of execution         |


## Change log (reverse chronological order)
***

Aug 2, 2020 : Work with frame in Selenium3

Jul 19, 2020 : Using Javascript executor in Selenium3

Jul 12, 2020 : Regular Actions in Selenium3

Jul 05, 2020 : Implicit, Explicit, & Fluent Wait in Selenium3

Jun 28, 2020 : Using Xpath with Selenium3

Jun 21, 2020 : Find an Element with Selenium3

Jun 15, 2020 : Open/Close a Browser with Selenium3

Jun 7, 2020 : Executing testcases parallel with TestNG

May 31, 2020 : Executing the specific testcase with TestNG

May 25, 2020 : Executing same testcases mutli-times with TestNG

May 17, 2020 : Ordering the execution of testcases with TestNG

May 10, 2020 : initial checked in,  Creating a single test with TestNG