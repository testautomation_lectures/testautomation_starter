package ch403;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class XpathComparison {
  
	@Test
	public void hitGoogle() {
		
		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		
		//wait little bit
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String title = driver.getTitle();
        System.out.println("page tile: "+ title);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        String id = driver.findElement(By.xpath("html/body/div/div/div/div/div/div[2]/div[1][@id='gbwa']")).getAttribute("id");
        System.out.println("id by absoulte xpath : " + id);
        
        id = driver.findElement(By.xpath("//*[@id='gbwa']")).getAttribute("id");
        System.out.println("id by relative xpath : " + id);
        
        WebElement element = driver.findElement(By.xpath("//*[@id='tsf']/div[2]/div[1]/div[1]/div/div[2]/input"));
        element.sendKeys("test");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        element.sendKeys(Keys.RETURN);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

}
