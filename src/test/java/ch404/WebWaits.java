package ch404;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class WebWaits {
  
	@Test
	public void hitGoogle_implicitwait() {
		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		long startTime = System.currentTimeMillis();
		driver.get("https://www.google.com/");
		
        String id = driver.findElement(By.xpath("//*[@id='gbwa']")).getAttribute("id");
        System.out.println("id by relative xpath : " + id);
        long endTime = System.currentTimeMillis();
        System.out.println("The implicitwait took " + (endTime - startTime) + " milliseconds");
        
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

	@Test
	public void hitGoogle_explicitwait() {
		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		long startTime = System.currentTimeMillis();
		driver.get("https://www.google.com/");
		
        String id = (new WebDriverWait(driver, 10))
        	    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='gbwa']"))).getAttribute("id");

        System.out.println("id by relative xpath : " + id);
        long endTime = System.currentTimeMillis();
        System.out.println("The explicitwait took " + (endTime - startTime) + " milliseconds");
        
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

	@Test
	public void hitGoogle_fluenttwait() {
		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		long startTime = System.currentTimeMillis();
		driver.get("https://www.google.com/");
		
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
        	    .withTimeout(Duration.ofSeconds(10))
        	    .pollingEvery(Duration.ofMillis(200))
        	    .ignoring(NoSuchElementException.class);
        
        String id = wait.until(new Function<WebDriver, WebElement>() 
        {
            public WebElement apply(WebDriver driver) {
            return driver.findElement(By.xpath("//*[@id='gbwa']"));
        }
        }).getAttribute("id");
        
        System.out.println("id by relative xpath : " + id);
        long endTime = System.currentTimeMillis();
        System.out.println("The fluenttwait took " + (endTime - startTime) + " milliseconds");
        
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

}
