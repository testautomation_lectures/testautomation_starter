package ch303;

import org.testng.annotations.Test;

public class RepeatedTest {
  
	@Test(invocationCount = 3)
	public void testA() {
		System.out.println("testA()");
	}

	/*
	   testA()
	   testA()
	   testA()
	 */
}
