package ch405;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common.SeleniumWebUtils;
import Common.TestBase;

public class RegulaActions {
  
	@Test
	@Parameters({ "browser" })
	public void hitGoogle(String browser) {
		
		//Set timeout value to 10 seconds as maximum
		long timeout = 10L;
		
		//Create Driver
		WebDriver driver = TestBase.createDriver(browser);
		//Maximize window
		driver.manage().window().maximize();
		//Open Browser
		driver.get("https://www.google.com/");
		
		//find the search box
        WebElement element = SeleniumWebUtils.findElementXpath(driver, "//*[@id='tsf']/div[2]/div[1]/div[1]/div/div[2]/input", timeout);
        
        // Type "test" + [Enter]
        SeleniumWebUtils.sendKeys(element, "test");
        SeleniumWebUtils.sendKeys(element, Keys.RETURN);
        
        SeleniumWebUtils.sleep(5000);
      
        // scrolling results
        SeleniumWebUtils.scroll(driver, 0, 500);
        SeleniumWebUtils.sleep(5000);
        
        //find "Fast.com" from results
        element = SeleniumWebUtils.findElementXpath(driver, "//a[h3[contains(.,'Fast.com')]]", timeout);
        //get attribute 
        System.out.println("url : " + SeleniumWebUtils.readAttribute(element, "href"));
        
        //get text 
        WebElement element1 = SeleniumWebUtils.findElementXpath(driver, "//a[h3[contains(.,'Fast.com')]]/h3", timeout);
        System.out.println("linktext : " + SeleniumWebUtils.read(element1));
        
        //Click to enter Fast.com
        SeleniumWebUtils.click(element, driver);
        SeleniumWebUtils.sleep(5000);
		
        //Close Browser
        TestBase.closeBrowser(driver);
	}	

}
