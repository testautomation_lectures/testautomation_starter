package ch402;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FindWebElement {
  
	@Test
	public void hitGoogle() {
		
		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		
		//wait little bit
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String title = driver.getTitle();
        System.out.println("page tile: "+ title);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        String tagName = driver.findElement(By.id("gbwa")).getTagName();
        System.out.println("tagName by ID : " + tagName);

        tagName = driver.findElement(By.cssSelector("#gbwa")).getTagName();
        System.out.println("tagName by cssSelector : " + tagName);
        
        tagName = driver.findElement(By.xpath("//*[@id='gbwa']")).getTagName();
        System.out.println("tagName by xpath : " + tagName);
        
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

}
