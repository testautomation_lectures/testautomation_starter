package ch401;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;



public class OpenCloseIE {
  
	@SuppressWarnings("deprecation")
	@Test
	public void hitGoogle() {
		
		System.setProperty("webdriver.ie.driver","drivers/windows/IEDriverServer.exe");
		// Debug driver instance
		//System.setProperty("webdriver.ie.driver.loglevel","DEBUG");
		//System.setProperty("webdriver.ie.driver.logfile","drivers/windows/IEDriverServer.log");
		
		DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
		dc.setCapability("ignoreZoomSetting", true);
		dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		dc.setJavascriptEnabled(true);
		dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		dc.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());
		InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions().merge(dc);
		WebDriver driver = new InternetExplorerDriver(internetExplorerOptions);
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		
		//wait little bit
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.quit();
		/* cmd /c taskkill /f /im IEDriverServer.exe  */
	}	

}
