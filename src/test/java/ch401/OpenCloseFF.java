package ch401;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

public class OpenCloseFF {
  
	@Test
	public void hitGoogle() {
		
		System.setProperty("webdriver.gecko.driver","drivers/windows/geckodriver.exe");
        //if remote driver needs to be triggered
		//DesiredCapabilities dc = DesiredCapabilities.firefox();
		//dc.setCapability("marionette", true);
		//FirefoxOptions ffOptions = new FirefoxOptions().merge(dc);
		//WebDriver driver = new FirefoxDriver(ffOptions);
		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		
		//wait little bit
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

}
