package ch407;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common.SeleniumWebUtils;
import Common.TestBase;

public class HandleIframe {
  
	@Test(enabled = false)
	@Parameters({ "browser" })
	public void hitYoutubeError(String browser) {
		
		//Set timeout value to 10 seconds as maximum
		long timeout = 10L;
		
		//Create Driver
		WebDriver driver = TestBase.createDriver(browser);
		//Maximize window
		driver.manage().window().maximize();
		//Open Browser
		driver.get("https://www.google.com/");
		
		//find the search box
        WebElement element = SeleniumWebUtils.findElementXpath(driver, "//*[@id='gbwa']/div/a", timeout);
        
        SeleniumWebUtils.sleep(3000);
        SeleniumWebUtils.click(element, driver);
        SeleniumWebUtils.sleep(3000);

        WebElement elementA = SeleniumWebUtils.findElementXpath(driver, "/html/body/div/c-wiz/div/div/c-wiz/div/div/ul[1]/li[4]/a", timeout);
        
        SeleniumWebUtils.sleep(3000);
      
        //get text 
        System.out.println("button text : " + elementA.findElement(By.xpath("span")).getText());
        
        //Close Browser
        TestBase.closeBrowser(driver);
	}	

	@Test(enabled = true)
	@Parameters({ "browser" })
	public void hitYoutubeOK(String browser) {
		
		//Set timeout value to 10 seconds as maximum
		long timeout = 10L;
		
		//Create Driver
		WebDriver driver = TestBase.createDriver(browser);
		//Maximize window
		driver.manage().window().maximize();
		//Open Browser
		driver.get("https://www.google.com/");
		
		//find the search box
        WebElement element = SeleniumWebUtils.findElementXpath(driver, "//*[@id='gbwa']/div/a", timeout);
        
        SeleniumWebUtils.sleep(3000);
        SeleniumWebUtils.click(element, driver);
        SeleniumWebUtils.sleep(3000);

        // Change frame to select correct element in iframe
        //Here are some codes to select an iframe using absolute value, this works sometimes not always
        
        //driver.switchTo().frame(1); SeleniumWebUtils.sleep(3000); // set frame requires processing time to select   
        
        //driver = SeleniumWebUtils.findFrame(driver, 1, 3L);       // this added expected condition, but this works sometimes not always
        
        //driver = SeleniumWebUtils.switchFrameByXpath(driver, "/html/body/div/c-wiz/div/div/c-wiz/div/div/ul[1]/li[4]/a", 3L); //Select iframe using element xpath, it always works
        
        List<WebElement> pathToFrame = SeleniumWebUtils.getFramePath(driver, "https://ogs.google.com/widget/app/so?origin=chrome-search%3A%2F%2Flocal-ntp&pid=1&spid=243&hl=en", "iframe");
        driver = SeleniumWebUtils.switchToFrameByPath(driver, pathToFrame, 3L);
        
        driver = SeleniumWebUtils.switchFrameByXpath(driver, "/html/body/div/c-wiz/div/div/c-wiz/div/div/ul[1]/li[4]/a", 3L); //Select iframe using element xpath, it always works
        
        WebElement elementA = SeleniumWebUtils.findElementXpath(driver, "/html/body/div/c-wiz/div/div/c-wiz/div/div/ul[1]/li[4]/a", timeout);
        
        SeleniumWebUtils.sleep(3000);
      
        //get text 
        System.out.println("button text : " + elementA.findElement(By.xpath("span")).getText());
        
        //Close Browser
        TestBase.closeBrowser(driver);
	}	
}
