package Common;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SeleniumWebUtils {

	private static final List<Class<? extends Exception>> IGNORED_EXCEPTIONS = new ArrayList<>();
	static {
		IGNORED_EXCEPTIONS.add(StaleElementReferenceException.class);
	}
	
	private static final int POLLING_TIME = 1000;

	/**
	 * Wait for condition usign the fluent driver
	 * 
	 * @param waitCondition
	 */
	private static <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition, long timeout) {
		return new WebDriverWait(webDriver, timeout, POLLING_TIME).until(waitCondition);
	}

	/**
	 * Waits for the specified condition until default timeout, ignoring exception
	 */
	private static <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition,
			List<Class<? extends Exception>> exceptionsToIgnore, long timeout) {
		return new WebDriverWait(webDriver, timeout, POLLING_TIME).ignoreAll(exceptionsToIgnore).until(waitCondition);
	}
	
	public static boolean waitForLoading(WebDriver driver, By locator, long timeout) {
		try {
			/*
			 * new WebDriverWait(driver,
			 * timeout).ignoring(StaleElementReferenceException.class)
			 * .until(ExpectedConditions.invisibilityOfElementLocated(locator));
			 */
			try {
				waitFor(driver, ExpectedConditions.invisibilityOfElementLocated(locator), timeout);
			} catch (StaleElementReferenceException sexcp) {

			}
			return true;
		} catch (TimeoutException e) {
			return false; // element not found
		}
	}
    
	public static void waitForLoad(WebDriver _driver, long timeout) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver _driver) {
				return ((JavascriptExecutor) _driver).executeScript("return document.readyState").equals("complete");
			}
		};
		//_driver = _driver.switchTo().defaultContent();
		 (new WebDriverWait(_driver, timeout)).until(pageLoadCondition);
	}

	public static WebElement findElement(WebDriver driver, By by, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfElementLocated(by), IGNORED_EXCEPTIONS, timeout);
	}

	public static WebElement findElement(WebDriver driver, WebElement element, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOf(element), IGNORED_EXCEPTIONS, timeout);
	}

    public static WebElement findElementXpath(WebDriver driver, String xpath, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> findElementsXpath(WebDriver driver, String xpath, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> findElements(WebDriver driver, By by, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(by), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> getElements(WebDriver driver, WebElement element, String xpathExpression, long timeout) {
    	if (findElement(driver, element, timeout) != null)
    		return element.findElements(By.xpath(xpathExpression));
    	else 
    		return null;
    }

    public static WebElement getElement(WebDriver driver, WebElement element, String xpathExpression, long timeout) {
    	if (findElement(driver, element, timeout) != null)
    		return element.findElement(By.xpath(xpathExpression));
    	else 
    		return null;
    }
    
    public static void clear(WebElement element) {
        element.clear();
    }

    public static void click(WebElement element, WebDriver driver) {
        // focus element before click when running in browser.
        focusElement(element, driver);
        element.click();
    }
    
    public static String read(WebElement element) {
        String text = null;

        // read the element text
        if ("input".equalsIgnoreCase(element.getTagName())) {
            text = element.getAttribute("value");
        } else {
            text = element.getText();
        }

        // sometime weblement.getText() returns no text while text is
        // present. check innerText in such cases
        if (text == null) {
            text = element.getAttribute("innerText");
        }

        return text;
    }

    public static String readAttribute(WebElement element, String attributeName) {
        String attribValue = element.getAttribute(attributeName);
        return attribValue;
    }

    public static void sendKeys(WebElement element, CharSequence... input) {
        // sendKeys doesn't work for readonly inputs.
        // updated a value attribute in such cases
        if ("input".equals(element.getTagName()) && element.getAttribute("readonly") != null) {
            JavascriptExecutor js = (JavascriptExecutor) ((WrapsDriver) element).getWrappedDriver();
            String script = String.format("arguments[0].setAttribute('value', '%s')", input.toString());
            js.executeScript(script.toString(), element);
        } else {
            element.sendKeys(input);
        }
    }

	public static void sleep(long msec)
	{
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
		}
	}
	
    public static void signatureOnWindow(WebDriver driver, WebElement canvasElement){
    	 Actions actionBuilder=new Actions(driver); 
    	 Action drawOnCanvas = actionBuilder 
    			 // .contextClick(canvasElement) 
    			 .moveToElement(canvasElement)
    			 //canvasElement is the element that holds the signature element you have in the DOM
    			 .clickAndHold() 
    			 .moveByOffset(100, 50)
    			 .moveByOffset(6,7)
    			 .moveByOffset(-15,-50)
    			 .moveByOffset(16,7)
    			 .release()
    			 .build(); 
    	 drawOnCanvas.perform();
    }
    
    public static void setTxtbox(WebDriver driver, WebElement elementInput, String _input){
        click(elementInput, driver);
    	//elementInput.click();
    	elementInput.clear();
        if (!(_input == null || _input.isEmpty())) {
        	elementInput.sendKeys(_input);
        }
    }

	public static void dragAndDrop(WebDriver driver, WebElement sourceElement, WebElement destinationElement) {
		(new Actions(driver)).dragAndDrop(sourceElement, destinationElement).build().perform();
	}
	
	
	//==========================================================================================================================
	// Frame handles
	//==========================================================================================================================

	public static boolean isFramePresent(WebDriver driver, String frameLocator, long timeout) {
		try {
			switchFrame(driver, frameLocator, timeout);
			return true;
		} catch (NoSuchFrameException | StaleElementReferenceException e) {
			return false; // frame not found
		}
	}

	// wait for specific frame by name or id
	// this works only for one layer below
	public static WebDriver switchFrame(WebDriver driver, WebElement frame, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame), IGNORED_EXCEPTIONS, timeout);
	}
	
	public static WebDriver switchFrame(WebDriver driver, String frameLocator, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator), IGNORED_EXCEPTIONS, timeout);
	}
	
	public static WebDriver switchFrame(WebDriver driver, int frameLocator, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator), IGNORED_EXCEPTIONS, timeout);
	}

	public static WebDriver switchFrameByXpath(WebDriver driver, String xpath, long timeout) {
		int size = driver.findElements(By.tagName("iframe")).size();
		for(int i=0; i<size; i++){
			driver = switchFrame(driver, i, timeout);
			sleep(1000);
		    if (driver.findElements(By.xpath(xpath)).size() > 0)
		    	break;
		    else
		    	driver = driver.switchTo().defaultContent(); //switching back from the iframe
		}
		
		return driver;
	}
	
	public static List<WebElement> getFramePath(WebDriver driver, String frameName, String frameType) {
		// switch to the top level frame to traverse the frames tree
		driver.switchTo().defaultContent();
		List<WebElement> framePath = findFrames(driver, frameName, frameType);
		Collections.reverse(framePath);
		return framePath;
	}

	private static List<WebElement> findFrames(SearchContext context, String frameName, String frameType) {
		List<WebElement> requiredFramePath = new LinkedList<>();
		List<WebElement> frames = context.findElements(By.tagName(frameType));
		for (WebElement frame : frames) {
			if (frameName.equals(frame.getAttribute("id")) || frameName.equals(frame.getAttribute("name")) || frameName.equals(frame.getAttribute("src"))) {
				requiredFramePath.add(frame);
			} else {
				requiredFramePath.addAll(findFrames(frame, frameName, frameType));
			}
			if (!requiredFramePath.isEmpty()) {
				requiredFramePath.add(frame);
				break;
			}
		}
		return requiredFramePath;
	}

	public static WebDriver switchToFrameByPath(WebDriver driver, List<WebElement> pathToFrame, long timeout) {
		driver = driver.switchTo().defaultContent();
		for (WebElement frame : pathToFrame) {
			driver = switchFrame(driver, frame, timeout);
		}
		
		return driver;
	}

	
	//==========================================================================================================================
	// Java executor examples
	//==========================================================================================================================
	
    public static void focusElement(WebElement element, WebDriver driver) {
        // focus element using javascript focus()
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].focus();", element);
    }

	public static String getCurrentUrlJS(WebDriver driver)
    {
		//Fetching the URL of the site.		
        return ((JavascriptExecutor) driver).executeScript("return document.URL;").toString();
    }
	
	public static String getCurrentPageTitleJS(WebDriver driver)
    {
		//Method document.title fetch the Title name of the site. 		
        return ((JavascriptExecutor) driver).executeScript("return document.title;").toString();
    }

	public static void showAlert(WebDriver driver, String message)
    {
		//To generate Alert window using JavascriptExecutor. 			
		((JavascriptExecutor) driver).executeScript("alert('"+message+"');");    		
    }

	public static void navigate(WebDriver driver, String url)
    {
		//Navigate to new Page		
		((JavascriptExecutor) driver).executeScript("window.location = '"+url+"'");    		
    }

	// in Chrome browser, click always has been done on middle position of control
    // but, if control is located too closed with other contrls.
    // click may be disturbed by overlapped area.
    // in this case, you may see below message; 
    // "Element is not clickable at point (123, 123). Other element would receive the click:.."
    // refer to https://github.com/seleniumhq/selenium-google-code-issue-archive/issues/2766
    // following click method is avoid above problem because it's using JS object, not browser control.
    public static void clickJS(WebDriver driver, String id) {
        // focus element before click when running in browser.
    	((JavascriptExecutor) driver).executeScript("if( document.createEvent ) {var evt = document.createEvent('MouseEvents');evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);document.getElementById('" + id + "').dispatchEvent(evt);} else if( document.createEventObject ) {var evObj =document.createEventObject();document.getElementById('" + id + "').fireEvent('onclick', evObj);}", "");
    }

    
    public static void clickJS2(WebDriver driver, WebElement element) {
    	//Perform Click using JavascriptExecutor		
    	((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    
	public static void sleepJS(WebDriver driver, long msec)
    {
		//Call executeAsyncScript() method to wait for 5 seconds		
		((JavascriptExecutor) driver).executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], "+String.valueOf(msec)+");");		
    }
	
	
	public static String getCurrentFrameJS(WebDriver driver)
    {
		//Call executeAsyncScript() method to get current frame		
		return (String)((JavascriptExecutor) driver).executeScript("return self.name");		
    }
	
	
	public static void scroll(WebDriver driver, int x, int y)
	{
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
		    jse.executeScript("window.scrollBy(" + x + "," + y + ")", "");
		} catch (Exception e) {
		}
	}

    
	public static String getComputedStyle(WebDriver driver, WebElement element, String attribute, String psedo)
	{
	    try
	    {  
	    	return (String)((JavascriptExecutor) driver).executeScript("return window.getComputedStyle(arguments[0], '"+psedo+"').getPropertyValue('"+ attribute +"');", element);
	    }
	    catch (Exception e)
	    {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	public static String getAbsoluteXPath(WebDriver driver, WebElement element)
    {
        return (String) ((JavascriptExecutor) driver).executeScript(
                "function absoluteXPath(element) {"+
                     "var comp, comps = [];"+
                     "var parent = null;"+
                     "var xpath = '';"+
                     "var getPos = function(element) {"+
                     "var position = 1, curNode;"+
                     "if (element.nodeType == Node.ATTRIBUTE_NODE) {"+
                        "return null;"+
                     "}"+
                     "for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling){"+
                        "if (curNode.nodeName == element.nodeName) {"+
                        	"++position;"+
                        "}"+
                     "}"+
                     "return position;"+
                 "};"+
                 "if (element instanceof Document) {"+
                 	"return '/';"+
                 "}"+

    			"for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {"+
    				"comp = comps[comps.length] = {};"+
    				"switch (element.nodeType) {"+
    					"case Node.TEXT_NODE:"+
    						"comp.name = 'text()';"+
    						"break;"+
    					"case Node.ATTRIBUTE_NODE:"+
    						"comp.name = '@' + element.nodeName;"+
    						"break;"+
    					"case Node.PROCESSING_INSTRUCTION_NODE:"+
    						"comp.name = 'processing-instruction()';"+
    						"break;"+
    					"case Node.COMMENT_NODE:"+
    						"comp.name = 'comment()';"+
    						"break;"+
    					"case Node.ELEMENT_NODE:"+
    						"comp.name = element.nodeName;"+
    						"break;"+
    				"}"+
    				"comp.position = getPos(element);"+
    			"}"+

    			"for (var i = comps.length - 1; i >= 0; i--) {"+
    				"comp = comps[i];"+
    				"xpath += '/' + comp.name.toLowerCase();"+
    				"if (comp.position !== null) {"+
    					"xpath += '[' + comp.position + ']';"+
    				"}"+
    			"}"+

    			"return xpath;"+

			"} return absoluteXPath(arguments[0]);", element);
    }

}
