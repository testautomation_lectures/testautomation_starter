package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestBase {
	public static WebDriver createDriver(String browser) {

        System.out.println("browser : " + browser);

        switch (browser)
        {
            case "IE":
        		System.setProperty("webdriver.ie.driver","drivers/windows/IEDriverServer.exe");
        		DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
        		dc.setCapability("ignoreZoomSetting", true);
        		dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        		dc.setJavascriptEnabled(true);
        		dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        		dc.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());
        		InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions().merge(dc);
        		return new InternetExplorerDriver(internetExplorerOptions);
            case "Edge":
        		System.setProperty("webdriver.edge.driver","drivers/windows/msedgedriver.exe");
        		return new EdgeDriver();
            case "FF":
        		System.setProperty("webdriver.gecko.driver","drivers/windows/geckodriver.exe");
         		return new FirefoxDriver();
            default:
        		System.setProperty("webdriver.chrome.driver","drivers/windows/chromedriver.exe");
        		return new ChromeDriver();
        }
    }
	
	
	public static void closeBrowser(WebDriver driver) {
		driver.quit();
	}
}
