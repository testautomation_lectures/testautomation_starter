package ch304;

import org.testng.annotations.Test;

public class SelectedTest1 {
  
	@Test(enabled = false)
	public void testA() {
		System.out.println("testA()");
	}

	@Test(enabled = true)
	public void testB() {
		System.out.println("testB()");
	}

	@Test(enabled = false)
	public void testC() {
		System.out.println("testC()");
	}

	/*
	  	testA()
	  	testC()
	 */
}
