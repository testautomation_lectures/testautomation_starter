package ch304;

import org.testng.annotations.Test;

public class SelectedTest2 {
  
	@Test(groups = {"Sanity", "Regression"})
	public void testWeb() {
		System.out.println("testWeb()");
	}

	@Test(groups = "Sanity")
	public void testMobile() {
		System.out.println("testMobile()");
	}

	@Test(groups = "Regression")
	public void testAPI() {
		System.out.println(" testAPI()");
	}
	
	/*
		testMobile()
		testWeb()
	 */
}
