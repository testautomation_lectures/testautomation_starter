package ch302;

import org.testng.annotations.Test;

public class OrderedTest2 {
  
	@Test(dependsOnMethods = { "testB" })
	public void testA() {
		System.out.println("testA()");
	}

	@Test()
	public void testB() {
		System.out.println("testB()");
	}

	@Test(dependsOnMethods = { "testA" })
	public void testC() {
		System.out.println("testC()");
	}

/* 
 * testB()
 * testA()
 * testC()
 */

}
