package ch302;

import org.testng.annotations.Test;

public class OrderedTest1 {
  
	@Test(priority = 1)
	public void testA() {
		System.out.println("testA()");
	}

	@Test(priority = 3)
	public void testB() {
		System.out.println("testB()");
	}

	@Test(priority = 2)
	public void testC() {
		System.out.println("testC()");
	}

	
	/* 
	 * testA()
	 * testC()
	 * testB()
	 */
}
